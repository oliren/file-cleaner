#include "configdial.h"
#include "ui_configdial.h"
#include <QInputDialog>
#include <QDebug>

ConfigDial::ConfigDial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDial)
{
    settings=new QSettings("settings.conf",QSettings::IniFormat);
    connect(this,SIGNAL(accepted()),SLOT(save()));
    //connect(ui->commandLinkButton,SIGNAL(clicked()),SLOT(setPwd()));
    ui->setupUi(this);
    foreach (QFileInfo val, QDir::drives())
    {
        if(val.absolutePath()!=QDir::rootPath())
        ui->mnt->addItem(val.absolutePath());
    }
    if(!settings->value("/Settings/root").isNull())
        if(!ui->mnt->findItems(settings->value("/Settings/root").toString(),Qt::MatchFixedString).isEmpty())
        {QListWidgetItem* curr= ui->mnt->findItems(settings->value("/Settings/root").toString(),Qt::MatchFixedString).first();
        if(curr->text()!="")
        ui->mnt->setCurrentItem(curr);
        }
    ui->commandLinkButton->setVisible(false);
}

ConfigDial::~ConfigDial()
{
    delete ui;
    delete settings;
}
void ConfigDial::save()
{

    settings->beginGroup("/Settings");
    if(settings->value("/root").toString()!=ui->mnt->currentItem()->text())
    {
        settings->setValue("/root",ui->mnt->currentItem()->text());
        emit changed();
    }


    settings->endGroup();
}
/*void ConfigDial::setpwd()
{
    QString pwd;
    QString newpwd;
    QString re;
    bool ok;
    if(!settings->value("/Settings/pwd").isNull())
        pwd=settings->value("/Settings/pwd").toString();
    if(pwd==QInputDialog::getText(this,tr("Ввод пароля"),tr("Введите старый пароль"),QLineEdit::Password,"",&ok))
    {
        newpwd=QInputDialog::getText(this,tr("Ввод пароля"),tr("Введите новый пароль"),QLineEdit::Password,"",&ok);
        re=QInputDialog::getText(this,tr("Ввод пароля"),tr("Повторите пароль"),QLineEdit::Password,"",&ok);
        if(ok && !re.isEmpty() && newpwd==re)
            settings->setValue("/Settings/pwd",newpwd);
    }
}
*/
