#include "clear.h"
#include <QDebug>



Clear::Clear(QString _path, QStringList _fixedDir)
{
        path = new QDir(_path);        
        setDir(_fixedDir);

}
bool Clear::setDir(QStringList _fixedDir)
{       if(_fixedDir.count()==0)
        return false;
        else
        {
        fix=_fixedDir;
        QFileInfoList fls=path->entryInfoList(QDir::Dirs | QDir::AllDirs | QDir::NoDotAndDotDot);
        foreach (QFileInfo dir, fls) {
            Dirs.append(QDir::convertSeparators(dir.absoluteFilePath()));

        }

        foreach(QString curr, _fixedDir)
            {
                Dirs.removeOne(QDir::convertSeparators(curr));



            }
            return true;
        }

}

void Clear::process()
{   stoped=false;
    foreach (QString pr, Dirs)
    {
        if(!removeDir(pr) && stoped)
        {
            emit message("stoped!");
            emit finished();
            break;
        }
    }
    emit message("done.");
    emit finished();

}
void Clear::stop()
{
    stoped=true;
}

bool Clear::removeDir(const QDir &dir)
{
        //Проверяем существует ли директория



        if(!dir.exists() || fix.count(QDir::convertSeparators(dir.absolutePath()))!=0 || stoped)
        {

            return false;
        }


        emit message("open:"+dir.absolutePath());
        //Получаем список каталогов
        QStringList lstDirs  = dir.entryList(QDir::Dirs  |

                                       QDir::AllDirs |
                                       QDir::NoDotAndDotDot);
       //Получаем список файлов
       QStringList lstFiles = dir.entryList(QDir::Files);

       //Удаляем файлы
       foreach (QString entry, lstFiles)
       {
          QString entryAbsPath = dir.absolutePath() + "/" + entry;

          QFile::remove(entryAbsPath);
          emit message("remove file:"+entryAbsPath);
       }

       //Для папок делаем рекурсивный вызов
       foreach (QString entry, lstDirs)
       {

          QString entryAbsPath = dir.absolutePath() + "/" + entry;

          if(!removeDir(entryAbsPath)&&stoped)
          {
              return false;
          }
       }

       //Удаляем обрабатываемую папку
       if (!QDir().rmdir(dir.absolutePath()))
       {

       }
       emit message("remove:"+dir.absolutePath());
       return true;
}
Clear::~Clear()
{
    if(path!=NULL)
    delete path;
}
