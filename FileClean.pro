#-------------------------------------------------
#
# Project created by QtCreator 2013-04-19T13:30:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FileClean
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    clear.cpp \
    dscan.cpp \
    configdial.cpp

HEADERS  += mainwindow.h \
    clear.h \
    dscan.h \
    configdial.h

FORMS    += mainwindow.ui \
    dscan.ui \
    configdial.ui \
    help.ui
CONFIG += thread

RESOURCES += \
    img.qrc
