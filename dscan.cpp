#include "dscan.h"
#include "ui_dscan.h"
#include "clear.h"
#include <QThread>
#include <QCloseEvent>
#include <QDebug>

dScan::dScan(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dScan)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Tool|Qt::WindowTitleHint);


}

dScan::~dScan()
{
    delete ui;
}
void dScan::AddThread(QString _path, QStringList _fixeddirs)
{
    Clear* worker = new Clear(_path,_fixeddirs);
    QThread * thread = new QThread;
    worker->moveToThread(thread);
    connect(thread, SIGNAL(started()), worker, SLOT(process()));//запуск обра6отки
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));//завершение обработки
    connect(ui->pbStop, SIGNAL(clicked()), worker, SLOT(stop()));//нажатие на кнопку стоп
    connect(worker,SIGNAL(message(QString)),ui->tConsole,SLOT(append(QString)));
    connect(worker,SIGNAL(message(QString)),this,SLOT(update()));
    //освобождение памяти
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(worker, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(worker, SIGNAL(finished()), this, SLOT(finish()));
    //запуск потока
    thread->start(QThread::HighPriority);
}
void dScan::setInput(QString p, QStringList l)
{   AddThread(p,l);

}

void dScan::finish()
{
    ui->pbOk->setEnabled(true);
}

void dScan::closeEvent(QCloseEvent * event)
{
    if(ui->pbOk->isEnabled())
        event->accept();
}
