#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    
private slots:
    void setting();
    void starting();
    void addPath();
    void removePath();
    void help();
    void clear();
    void about();
    //void setPass();

private:
    Ui::MainWindow *ui;
    QStringList fix;
    QSettings * settings;
};

#endif // MAINWINDOW_H
