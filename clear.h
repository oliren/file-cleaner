#ifndef CLEAR_H
#define CLEAR_H
#include <QDir>


class Clear : public QObject
{
    Q_OBJECT
public:

    Clear(QString _path, QStringList _fixedDir);
    ~Clear();
public slots:
    void process();
    void stop();
signals:
    void finished();
    void message(QString path);

private:
    bool removeDir(const QDir &dir);
    bool setDir(QStringList _fixedDir);
    QStringList Dirs;
    QStringList fix;
    QDir * path;
    volatile bool stoped;

};

#endif // CLEAR_H
