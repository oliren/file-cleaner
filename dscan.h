#ifndef DSCAN_H
#define DSCAN_H

#include <QDialog>


namespace Ui {
class dScan;
}

class dScan : public QDialog
{
    Q_OBJECT
    
public:
    explicit dScan(QWidget *parent = 0);
    ~dScan();
public slots:
    void setInput(QString,QStringList);
    void finish();


private:
    Ui::dScan *ui;
    void AddThread(QString _path, QStringList _fixeddirs);
    virtual void closeEvent(QCloseEvent *event);
};

#endif // DSCAN_H
