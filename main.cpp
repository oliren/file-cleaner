#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{   QTranslator * qt_translator = new QTranslator();
    if (!qt_translator->load(":/help/qt_ru.qm"))
    {
      delete qt_translator;
      return false;
    }
    QApplication a(argc, argv);
    a.installTranslator(qt_translator);
    MainWindow w;
    QCoreApplication::setOrganizationName("Lockirin");
    QCoreApplication::setApplicationName("FileCleaner");
    w.show();
    
    return a.exec();
}
