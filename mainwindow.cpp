#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "configdial.h"
#include "dscan.h"
#include "QFileDialog"
#include "ui_help.h"
#include <QDebug>
#include <QMessageBox>
#include <QTextCodec>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   settings=new QSettings("settings.conf",QSettings::IniFormat);

    ui->setupUi(this);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    connect(ui->action,SIGNAL(triggered()),this,SLOT(starting()));
    connect(ui->action_4, SIGNAL(triggered()),this,SLOT(addPath()));
    connect(ui->action_5,SIGNAL(triggered()),SLOT(removePath()));
    connect(ui->action_3,SIGNAL(triggered()),SLOT(setting()));
    connect(ui->action_6,SIGNAL(triggered()),SLOT(help()));
    connect(ui->action_7,SIGNAL(triggered()),SLOT(about()));
    fix=settings->value("/Settings/fixlist").toStringList();
    //instal model    
    ui->listdir->addItems(fix);
    if (ui->listdir->count()==0 || settings->value("/Settings/root").isNull())
        ui->action->setEnabled(false);
    int result=QMessageBox::question(this,"Warning!",tr("Внимание! разработчики программы не несут"
                                                        " ответственности за последствия использования программы, перед использованием ознакомтесь с справочной системой!"
                                                        "<p><b> Вы используете программу на свой страх и риск</b>"
                                                        "<p> "
                                                        " Вы действительно осознаете последствия выполнения программы?"
                                                        ),QMessageBox::Yes,QMessageBox::No);

    if(result==QMessageBox::No)
        exit(0);

}

MainWindow::~MainWindow()
{
    settings->setValue("/Settings/fixlist",fix);
    settings->sync();
    delete ui;
    delete settings;

}

void MainWindow::setting()
{
    ConfigDial  *settingsDial = new ConfigDial(this);
    connect(settingsDial,SIGNAL(changed()),ui->listdir,SLOT(clear()));
    settingsDial->exec();

}
void MainWindow::starting()
{
    if (ui->listdir->count()!=0 && !settings->value("/Settings/root").isNull()){
        int res=QMessageBox::question(0, tr("Внимание"), tr("Вы действительно хотите выполнить операцию?<br>"
                                                            "<font color='red'><b>Внимание операция удалит ваши файлы!!!</b></font>"
                                                            "<p>Перед началом операции настоятельно рекомендуем завершить все исполняемые программы!")
                                      ,QMessageBox::Yes,QMessageBox::No);
        switch(res){
        case QMessageBox::Yes:
            dScan *scan = new dScan(this);
            scan->setInput(settings->value("/Settings/root").toString(),
                   fix);
            this->hide();
            scan->setWindowState(Qt::WindowMaximized);
            scan->exec();
            clear();
            this->show();
            break;

    }
        }



}
void MainWindow::addPath()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                                                          tr("Directory"),
                                                          settings->value("/Settings/root").toString());
    if(ui->listdir->findItems(directory,Qt::MatchFixedString).count()==0)
        if(directory!="" && directory!=QDir::convertSeparators(settings->value("/Settings/root").toString())){
             ui->listdir->addItem(directory);
             fix.append(directory);
             ui->action->setEnabled(true);
        }

}

void MainWindow::removePath()
{
    fix.removeOne(ui->listdir->currentItem()->text());
    delete ui->listdir->currentItem();
    if(fix.count()==0)
        ui->action->setEnabled(false);

}

void MainWindow::help()
{
    QDialog *help = new QDialog(this,Qt::WindowCloseButtonHint);
    Ui::Form ui;
    ui.setupUi(help);
    ui.textBrowser->setSource(QUrl("qrc:/help/help/index.html"));
    ui.textBrowser->setSearchPaths(QStringList()<<QApplication::applicationDirPath()+"/img");
    help->exec();

}
void MainWindow::clear()
{
    ui->action->setEnabled(false);
    fix.clear();
    ui->listdir->clear();
    settings->setValue("Settings/root",QVariant(QString::null));
}
void MainWindow::about()
{
    QMessageBox::about(this, tr("About FileClean"),
              tr("<h2>FileClean 1.0</h2>"
              "<p>Copyright &copy; 2013 Software Inc."
              "<p>FileClean предназначен для удаления "
              "файлов и каталогов, и поддержания файловой иерархии. "
              "<p>Проект разработан студентом группы РПЗ-09 1/9<br>"
              "Кандул Андреем"));
}
