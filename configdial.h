#ifndef CONFIGDIAL_H
#define CONFIGDIAL_H
#include <QDir>
#include <QDialog>
#include <QSettings>

namespace Ui {
class ConfigDial;
}

class ConfigDial : public QDialog
{
    Q_OBJECT
    
public:
    explicit ConfigDial(QWidget *parent = 0);
    ~ConfigDial();
signals:
    void changed();
public slots:
    void save();
private:
    Ui::ConfigDial *ui;
    volatile bool configure;
    QSettings *settings;
};

#endif // CONFIGDIAL_H
